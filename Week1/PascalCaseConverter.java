import java.util.Scanner;

/*

Converts a string to PascalCase

removes all none-alfabetical characters from the string, end treats them like an empty space

*/

class PascalCaseConverter{

    public static String convertToPC(String str){

        str = str.toLowerCase();
        char[] charay = new char[str.length()]; 
  
        // Copy character by character into array 
        for (int i = 0; i < str.length(); i++) { 
            charay[i] = str.charAt(i); 
        }

        String pascaleCase = "";
        boolean isCapital = true;

        for (int i = 0; i < charay.length; i++){
            if(!Character.isLetter(charay[i])){
                charay[i] = ' ';
            }

            if(charay[i] == ' '){
                isCapital = true;
            }else if(isCapital && Character.isLetter(charay[i])){
                pascaleCase += Character.toUpperCase(charay[i]);
                isCapital = false;
            }else{
                pascaleCase += charay[i];
                isCapital = false;
            }
        }
        return pascaleCase;
    }

    public static void main(String[]arg){

        boolean LETMEOUT = false;

        while(!LETMEOUT){

            System.out.println("Enter phrase:\n");

            Scanner scanner = new Scanner(System.in);
            String input = scanner.nextLine();

            if (input == ""){
                LETMEOUT = true;
            }else{

                String converted = convertToPC(input);
                System.out.println("\n\n\n" + input + " \n CONVERTED TO:\n" + converted);

            }
        }
    }
}

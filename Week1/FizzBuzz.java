public class FizzBuzz {


    public static void fizzbuzz(int max){
        
        for (int i = 1; i <= max; i++){

            String output = "";

            if(i % 3 != 0 && i % 5 != 0){
                output += i;
            }else{
                if(i % 3 == 0){
                    output += "Fizz";
                }
                if(i % 5 == 0){
                    output += "Buzz";
                }

            }
            System.out.println(output);
        }

    }

    public static void main(String [] arg){
        fizzbuzz(100);
    }
    
}

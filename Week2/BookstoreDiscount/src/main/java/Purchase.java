
public class Purchase {

    public double price(int[] chart){
        double price = 8.0;
        int count = 0;
        for(int i: chart){
            count += i;
        }
        return calculateDiscount(chart, count * price);
    }

    private double calculateDiscount(int[] chart, double price){
        double discount = 0.0;
        int types = 0;
        for (int i: chart) {
            if (i > 0){
                types++;
            }
        }
        switch (types) {
            case 2 -> discount = 16 - (16 * 0.95);
            case 3 -> discount = 8 * 3 - (8 * 3 * 0.9);
            case 4 -> discount = 8 * 4 - (8 * 4 * 0.8);
        }
        return price-discount;
    }
}

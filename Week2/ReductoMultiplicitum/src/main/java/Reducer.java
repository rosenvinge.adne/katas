import java.util.ArrayList;
import java.util.List;

public class Reducer {

    public static int sumDigProd(int... i){
        int sum = 0;
        for (int j:i){
            sum += j;
        }
        return multiplyDigits(sum);
    }

    private static int multiplyDigits(int digits){
        if (digits < 10){
            return digits;
        }else{
            List<Integer> splitDigits = toDigits(digits);

            if (splitDigits.size() == 1){
                return splitDigits.get(0);
            }else{
                int product = 1;
                for (int i:splitDigits) {
                    product = product*i;
                }
                return multiplyDigits(product);
            }
        }
    }

    private static List<Integer> toDigits(int i) {
        List<Integer> digits = new ArrayList<>();
        while(i > 0) {
            digits.add(i % 10);
            i /= 10;
        }
        return digits;
    }
}

package EventCompetitors;

public class EventCompetitors {
    public static void main(String [] arg){
        Ironman ironMan = new Ironman("Ironman 1 - the phantom menece");

        ironMan.registerCompeditor(new Attendee("Annakin", 7), AttendeeType.ATHLETE);
        ironMan.registerCompeditor(new Attendee("Obi", 27), AttendeeType.ATHLETE);
        ironMan.registerCompeditor(new Attendee("Liam Neeson", 47), AttendeeType.ATHLETE);
        ironMan.registerCompeditor(new Attendee("Darth Maul", 87), AttendeeType.ATHLETE);
        ironMan.registerCompeditor(new Attendee("Annakins mother", 18), AttendeeType.SPECTATOR);
        ironMan.registerCompeditor(new Attendee("Palpatin", 92), AttendeeType.SPECTATOR);
        ironMan.registerCompeditor(new Attendee("Jabba", 352), AttendeeType.MARSHAL);

        System.out.println(ironMan.getEventName());
        System.out.println(ironMan.allAttendees());

        System.out.println("Children:");

        for (Attendee a: ironMan.getAllChildCompetitors()) {
            System.out.println("Competitor: " + a.getName() + " Age: " + a.getAge());
        }
        System.out.println("Competitors:");
        for (Attendee a : ironMan.getAllCompeditors()) {
            System.out.println("Competitor: " + a.getName() + " Age: " + a.getAge());
        }

        System.out.println("");
        


    }
    
}

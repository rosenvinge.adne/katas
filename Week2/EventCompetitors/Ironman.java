package EventCompetitors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Ironman implements Event {

    Attendee[] athletes;
    Attendee[] marshal;
    Attendee[] spectator;

    String eventName;
    HashMap<Attendee, AttendeeType> allAttendees = new HashMap<>();

    public Ironman(String eventName){
        this.eventName = eventName;
    }

    @Override
    public String getEventName(){
        return eventName;
    }

    @Override
    public void registerCompeditor(Attendee attendee, AttendeeType attendeeType) {
        // add to each list

    }

    @Override
    public ArrayList<Attendee> getAllCompeditors() {
        ArrayList compeditors = new ArrayList<Attendee>();

        for (Attendee a: allAttendees.keySet()) {
            if (a.getAge() > 16 && allAttendees.get(a) == AttendeeType.ATHLETE){
                compeditors.add(a);
            }
        }
        return compeditors;
    }

    @Override
    public ArrayList<Attendee> getAllChildCompetitors() {
        ArrayList compeditors = new ArrayList<Attendee>();
        
        for (Attendee a: allAttendees.keySet()) {
            if (a.getAge() < 16 && allAttendees.get(a) == AttendeeType.ATHLETE){
                compeditors.add(a);
            }
        }
        return compeditors;
    }

    @Override
    public Attendee[] allAttendees() {
        Attendee[] att = new Attendee[allAttendees.size()];

        int i = 0;
        for (Attendee a: allAttendees.keySet()){
            att[i] = a;
            i++;
        }
        return att;
    }
    
}

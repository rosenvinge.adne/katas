import java.time.DayOfWeek;
import java.time.LocalDate;

public class FridayTheThirteen {

    private static int yearContains(int yearStart, int yearEnd){
        int count = 0;

        for (int y = yearStart; y < yearEnd; y++){
            for (int m = 1; m < 12; m++) {
                LocalDate d = LocalDate.of(y, m, 13);
                if (d.getDayOfWeek() == DayOfWeek.FRIDAY){
                    count++;
                }
            }
        }

        return count;
    }


    public static void main(String[] arg){
        
        System.out.println(yearContains(2021, 3000));

    }
}

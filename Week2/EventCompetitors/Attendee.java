package EventCompetitors;

public class Attendee {
    String name;
    int age;
    public Attendee(String name, int age){
        this.name = name;
        this.age = age;
    } 
    public int getAge(){
        return age;
    }
    public String getName(){
        return name;
    }



}

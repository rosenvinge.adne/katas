package EventCompetitors;

public enum AttendeeType {
    MARSHAL,
    ATHLETE,
    SPECTATOR;
}

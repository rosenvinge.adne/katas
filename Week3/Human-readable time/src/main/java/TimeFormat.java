public class TimeFormat {

    /*
    
    Translates time from seconds to hh:mm:ss
    
    */

    public static String getReadableTime(int time){
        if (time < 1 || time > 359999) return "00:00:00";

        int h = 0;
        int m = 0;
        int s = time;

        while (s > 59){
            m++;
            s -= 60;
        }

        while (m > 59){
            m -= 60;
            h++;
        }
        
        String[] timeString = {h+":", m+":", s+""};
        if (timeString[0].length() < 3) timeString[0] = "0"+timeString[0];
        if (timeString[1].length() < 3) timeString[1] = "0"+timeString[1];
        if (timeString[2].length() < 2) timeString[2] = "0"+timeString[2];
        return timeString[0] + timeString[1] + timeString[2];

        /*

        StringBuilder retString = new StringBuilder("");

        if (h < 10) retString.append("0");
        if (h == 0) {
            retString.append("0:");
        }else{
            retString.append(h + ":");
        }

        if (m < 10) retString.append("0");
        if (m == 0) {
            retString.append("0:");
        }else{
            retString.append(m + ":");
        }
        if (s < 10) retString.append("0");
        if (s == 0) {
            retString.append("0");

        }else{
            retString.append(s);
        }

        return retString.toString();

         */

    }
}

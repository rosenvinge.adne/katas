class RemoveTheWord{

    /*
    
    From a array of characters remove all letters found in a given word

    */ 


    public static char[] removeLetters(char[] letters, String word){ 
        char[] toLetters = word.toCharArray();

        int toBeRemoved = 0;

        for (int i = 0; i < letters.length; i++) {
            for (int j = 0; j < toLetters.length; j++){
                if (letters[i] == toLetters[j] && letters[i] != ' '){
                    //System.out.print(letters[i] + " " + toBeRemoved);
                    letters[i] = ' ';
                    toLetters[j] = ' ';
                    toBeRemoved++;
                }
            }
        }

        char[] ret = new char[letters.length - toBeRemoved];
        //System.out.println(letters.length + " " + toBeRemoved + " " + ret.length);
        
        if (ret.length == 0) return ret;

        int i = 0;

        for (char c : letters) {
            if (c != ' ') {
                ret[i] = c;
                i++;
            }
        }
        return ret;
    }


    public static void printResults(char[] r){
        System.out.println("TEST: ");
        for (char c : r) {
            System.out.print(c + " ");
        }
        System.out.println();

    }

    public static void main(String [] arg){
        printResults(removeLetters(new char[] {'s', 't', 'r', 'i', 'n', 'g'}, "string"));

        printResults(removeLetters(new char[] {'s', 't', 'r', 'i', 'n', 'g', 'w'}, "string"));

        printResults(removeLetters(new char[] {'b', 'b', 'l', 'l', 'g', 'n', 'o', 'a', 'w'}, "balloon"));

        printResults(removeLetters(new char[] {'a', 'n', 'r', 'y', 'o', 'w'}, "norway"));

        printResults(removeLetters(new char[] {'t', 't', 'e', 's', 't', 'u'}, "testing"));
    }

}
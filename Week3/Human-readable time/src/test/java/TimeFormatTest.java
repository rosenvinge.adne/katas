import static org.junit.jupiter.api.Assertions.*;

class TimeFormatTest {

    @org.junit.jupiter.api.Test
    void getReadableTime() {

        assertEquals("00:00:00", TimeFormat.getReadableTime(0));
        assertEquals("00:00:05", TimeFormat.getReadableTime(5));
        assertEquals("00:01:00", TimeFormat.getReadableTime(60));
        assertEquals("23:59:59", TimeFormat.getReadableTime(86399));
        assertEquals("99:59:59", TimeFormat.getReadableTime(359999));

    }
}
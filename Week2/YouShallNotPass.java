class YouShallNotPass{

    private static boolean uppercase(String p){
        for (char c: p.toCharArray()){
            if  (Character.isUpperCase(c)){return true;}
        }
        System.out.print(" - NO uppercase - ");
        return false;
    }

    private static boolean lowercase(String p){
        for (char c: p.toCharArray()){
            if  (Character.isLowerCase(c)){return true;}
        }
        System.out.print(" - NO lowercase - ");
        return false;
    }

    private static boolean containsChar(String p){
        String specialCharacters = "!#$%&'()*+,-./:;<=>?@[]^_`{|}\"\\";
        for(int i=0 ; i < specialCharacters.length(); i++){
            //Checking if the input string contain any of the specified Characters
            if(p.contains(Character.toString(specialCharacters.charAt(i)))){
                return true;
            }
        }
        System.out.print(" - NO special character - ");

        return false;
    }

    private static boolean containsDigit(String p){
        for (char c: p.toCharArray()){
            if (Character.isDigit(c)){return true;}
        }
        System.out.print(" - NO digit - ");

        return false;
    }


    private static int checkPassword(String password){

        System.out.println("\n");

        int strength = 0;

        if (password.length() >= 8){strength++;}

        if (uppercase(password)){strength++;}

        if (lowercase(password)){strength ++;}

        if (containsChar(password)){strength++;}

        if (containsDigit(password)){strength++;}


        // Check if valid
        if (password.length() < 7 || password.contains(" ")){
            strength = 0;
            if (password.length() < 7){
                System.out.print(" - TO SHORT - ");
            }else{
                System.out.print(" - CAN NOT CONTAIN EMPTY SPACES - ");
            }   
        }

        if (strength == 0){
            System.out.println("\n"+ password + " -> Invalid");
        }else if(strength < 3){
            System.out.println("\n"+password + " -> Weak");
        }else if(strength < 5){
            System.out.println("\n"+password + " -> Moderate");
        }else{
            System.out.println("\n"+password + " -> Strong");
        }
        return strength;
    }






    public static void main(String [] arg){
        int s;
        s = checkPassword("password");
        s = checkPassword("11278274");
        s = checkPassword("@S3cur1ty");
        s = checkPassword("!@!pass1");
        s = checkPassword("mySecurePass123");
        s = checkPassword("pass word");
        s = checkPassword("stonk");
    }

}
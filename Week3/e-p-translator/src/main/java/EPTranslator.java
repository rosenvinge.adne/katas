import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EPTranslator {



    private static boolean isVowel(char ch){
        return "AEIOUaeiou".indexOf(ch) != -1;
    }

    private static boolean containsVowel(String word){
        char[] chars = word.toCharArray();
        for (char c:chars) {
            if (isVowel(c)) return true;
        }
        return false;
    }


    public static String translateWord(String word){
        boolean capitol = (!word.equals(word.toLowerCase()));
        word = word.toLowerCase();


        if (word.length() < 1) return "";

        boolean p = false;
        char punctuation = '.';

        if (word.charAt(word.length()-1) == '.' || word.charAt(word.length()-1) == '!' || word.charAt(word.length()-1) == '?') {
            p = true;
            punctuation = word.charAt(word.length()-1);
            word = word.substring(0, word.length()-1);
        }


        if (isVowel(word.charAt(0))){
            return word+"yay";
        }else{
            if (!containsVowel(word)) return "a" + word +"ay";

            while (!isVowel(word.charAt(0))) {
                word = word.substring(1, word.length()) + word.charAt(0);
            }
            word+= "ay";
        }
        if (capitol) word = word.substring(0, 1).toUpperCase() + word.substring(1);

        if (p) return word + punctuation;
        return word;
    }




    public static String translateSentence(String sentence){
        String[] words = sentence.split(" ");
        String result = "";

        for (String w:words) {
            result += translateWord(w) + " ";
        }
        return result;
    }

    public static void main(String[]arg){
        System.out.println(translateWord("have"));
        System.out.println(translateWord("cram"));
        System.out.println(translateWord("take"));
        System.out.println(translateWord("cat"));
        System.out.println(translateWord("shrimp"));
        System.out.println(translateWord("trebuchet"));
        System.out.println(translateWord(""));
        System.out.println(translateWord("ate"));
        System.out.println(translateWord("Apple"));
        System.out.println(translateWord("oaken"));
        System.out.println(translateWord("eagle"));

        System.out.println(translateSentence("I like to eat honey waffles."));
        System.out.println(translateSentence("Do you think it is going to rain today?"));

        System.out.println(translateWord("Eagle."));
    }

}

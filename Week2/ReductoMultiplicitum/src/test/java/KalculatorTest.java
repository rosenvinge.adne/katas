import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReducerTest {

    @Test
    public void sumDigProdTest(){

        assertEquals(0, Reducer.sumDigProd(0));
        assertEquals(9, Reducer.sumDigProd(9));
        assertEquals(7, Reducer.sumDigProd(9, 8));
        assertEquals(6, Reducer.sumDigProd(16, 28));
        assertEquals(1, Reducer.sumDigProd(111111111));
        assertEquals(2, Reducer.sumDigProd(1, 2, 3, 4, 5, 6));
        assertEquals(6, Reducer.sumDigProd(8, 16, 89, 3));
        assertEquals(6, Reducer.sumDigProd(26, 497, 62, 841));
        assertEquals(6, Reducer.sumDigProd(17737, 98723, 2));
        assertEquals(8, Reducer.sumDigProd(123, -99));
        assertEquals(8, Reducer.sumDigProd(167, 167, 167, 167, 167, 3));
        assertEquals(2, Reducer.sumDigProd(98526, 54, 863, 156489, 45, 6156));

    }
}
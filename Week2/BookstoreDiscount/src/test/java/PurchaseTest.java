import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PurchaseTest {

    @Test
    void priceTest() {
        int[][] ex = {
                {0, 0, 0, 0},
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 3},

                {1, 1, 0, 0},
                {1, 1, 1, 0},
                {1, 1, 1, 1},
        };

        Purchase p = new Purchase();

        assertEquals(0, p.price(ex[0]));
        assertEquals(8, p.price(ex[1]));
        assertEquals(8, p.price(ex[2]));
        assertEquals(8, p.price(ex[3]));
        assertEquals(8*3, p.price(ex[4]));

        assertEquals(8*2*0.95, p.price(ex[5]));
        assertEquals(8*3*0.9, p.price(ex[6]));
        assertEquals(8*4*0.8, p.price(ex[7]));


    }
}
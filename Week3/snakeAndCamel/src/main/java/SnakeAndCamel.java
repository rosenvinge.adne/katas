public class SnakeAndCamel {

    /*

    Create two methods, one to translate from Snake- to camelcase and one from camel- to snakecase

    */ 

    public static String toCamelCase(String string){
        char[] chars = string.toCharArray();

        StringBuilder camelCase = new StringBuilder();
        boolean nextIsCapital = false;

        for (char c:chars) {

            if (nextIsCapital && c!='_'){

                nextIsCapital = false;
                camelCase.append(Character.toUpperCase(c));

            }else if (c == '_') {

                nextIsCapital = true;

            }else{

                camelCase.append(c);
                nextIsCapital = false;

            }
        }
        return camelCase.toString();
    }
    public static String toSnakeCase(String string){

        char[] chars = string.toCharArray();
        StringBuilder snakeCase = new StringBuilder();

        for (char c:chars) {

            if (Character.isUpperCase(c)){

                snakeCase.append("_").append(Character.toLowerCase(c));

            }else{
                snakeCase.append(c);
            }
        }
        return snakeCase.toString();
    }

    public static void main(String [] args){
        System.out.println(toCamelCase("hello_rabbit"));
        System.out.println(toCamelCase("is_modal_open"));
        System.out.println(toCamelCase("get___color_"));

        System.out.println(toSnakeCase("helloRabbit"));
        System.out.println(toSnakeCase("getColor"));
        System.out.println(toSnakeCase("isModalOpen"));
    }
}

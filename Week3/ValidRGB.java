

class ValidRGB{

    private static boolean isRGB(String value){
        value = value.replace("\\(", "").replace("\\)", "");

        String[] split = value.split(",");

        for (String s : split) {
            if (! isNumeric(s)){
                return false;
            }else{
                if (Integer.parseInt(s)<=0 || Integer.parseInt(s)>=255){
                    return false;
                }
            }
            
        }
        return true;

    }

    private static boolean isRGBA(String value){
        value = value.replace("\\(", "").replace("\\)", "");

        String[] split = value.split(",");


        // Removes the a in rgba
        if (!isRGB(value.substring(0, value.length - split[split.length-1].length()+1))){
            return false;
        }

        if (!isNumeric(split[split.length-1])){
            return false;
        }
        return true;





    }



    public static boolean validRgbaColours(String string){
        string = string.replaceAll("\\s", "").toLowerCase();

        String colourType = string.substring(0, string.indexOf("(")-1);

        if (colourType.equals("rgb")){
            return isRGB(string.substring(string.indexOf("("), string.indexOf(")")));
        }

        if (colourType.equals("rgba")){
            return isRGBA(string.substring(string.indexOf("("), string.indexOf(")")));
        }
        return false;
    }


    public static void main(String[]arg){

        System.out.println(""+isNumeric(".123"));

        assertEquals(true, validRgbaColours("rgb(0,0,0)"));

    }

}
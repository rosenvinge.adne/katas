class LargestDifference{


    // How many pairs in 'num' add up tp 'target'
    private static void howManyPairs(int[] num, int target){
        for (int i = 0; i < num.length-1; i++) {
            for(int j = i+1; j < num.length; j++){
                if(num[i] + num[j] == target){
                    System.out.println(num[i]+num[j] + " = " + num[i] + "(" + i + ") + " + num[j] + "("+ j +")");
                }
            }
        }
    }


    //What is the largest difference between all the numbers in 'num', where the index og the smaller is smaller
    private static void findLargestDifference(int[] num){

        int largestSum = 0;
        int smallestIndex = 0;
        int largestIndex = 0;

        for (int i = 0; i < num.length-1; i++) {
            for (int j = i+1; j < num.length; j++){
                if(num[j]-num[i] > largestSum){
                    largestSum = num[j]-num[i];
                    smallestIndex = i;
                    largestIndex = j;
                }
            }
        }
        System.out.println(largestSum + " = " + num[largestIndex] + "("+largestIndex+") - " + num[smallestIndex] + "("+smallestIndex+")");
        
        howManyPairs(num, largestSum);
    }



    public static void main(String[]arg){

        int[] num = {2, 3, 1, 7, 9, 5, 11, 0, 3, 5};

        findLargestDifference(num);

    }

}

package EventCompetitors;

import java.util.ArrayList;

public interface Event {

    public String getEventName();
    public Attendee[] allAttendees();
    public void registerCompeditor(Attendee attendee, AttendeeType attendeeType);
    public ArrayList<Attendee> getAllCompeditors();
    public ArrayList<Attendee> getAllChildCompetitors();
}

public class Complete {

    /* 
        Check if a string can be completed  with a given string 
    */

    public static boolean canComplete(String str, String word){
        char[] strArray = str.toCharArray();
        char[] wordArray = word.toCharArray();

        int correct = 0;
        int pos = 0;

        for (char c : strArray) {

            int j = 0;
            boolean added = false; 

            while ((pos + j) < wordArray.length && !added) {
                if (c == wordArray[pos + j]) {
                    correct++;
                    pos += j;
                    j = 0;
                    added = true;
                }
                j++;
            }
        }

        return correct == strArray.length;
    }


    public static void main(String[] arg){
        System.out.println(canComplete("butl", "beautiful"));
        System.out.println(canComplete("butlz", "beautiful"));
        System.out.println(canComplete("tulb", "beautiful"));

        System.out.println(canComplete("beauti", "beautiful"));
        System.out.println(canComplete("beautiful", "beautiful"));
        System.out.println(canComplete("ababab", "abababab"));
        System.out.println(canComplete("abababc", "abababab"));

    }
}
